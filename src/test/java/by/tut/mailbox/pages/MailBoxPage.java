package by.tut.mailbox.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class MailBoxPage extends BasePage {

    private static final String INBOX_FOLDER_SELECTOR = "//span[text()=\"Входящие\"]";
    private static final String USERNAME_SELECTOR = "mail-User-Name";
    private static final String SENT_FOLDER_SELECTOR = "//a[@href=\"#sent\"]";
    private static final By FROM_FIELD = By.xpath("//span[@class='mail-MessageSnippet-FromText']");
    private static final By MAIL_SUBJECT_FIELD = By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_subject']/span");
    private static final By MAIL_BODY_FIELD = By.xpath("//span[@class='mail-MessageSnippet-Item mail-MessageSnippet-Item_firstline js-message-snippet-firstline']/span");


    public MailBoxPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = INBOX_FOLDER_SELECTOR)
    private WebElement inboxFolder;

    @FindBy(xpath = SENT_FOLDER_SELECTOR)
    private WebElement sentFolder;

    @FindBy(className = USERNAME_SELECTOR)
    private WebElement userName;

    public String getUserName() {
        return userName.getText();
    }

    public void switchToSent () {
        logger.info("Switch to 'Sent' folder.");
        waitForElementClickable(By.xpath(SENT_FOLDER_SELECTOR));
        sentFolder.click();
        waitForElementPresent(FROM_FIELD);
    }

    public void switchToInbox () {
        inboxFolder.click();
    }

    public String getMailRecipient() {
        logger.info("Get mail recipient name.");
        waitForElementPresent(FROM_FIELD);
        return driver.findElement(FROM_FIELD).getText();
    }

    public String getMailSubject() {
        logger.info("Get mail subject.");
        waitForElementPresent(MAIL_SUBJECT_FIELD);
        return driver.findElement(MAIL_SUBJECT_FIELD).getText();
    }

    public String getMailBody() {
        logger.info("Get mail body.");
        waitForElementPresent(MAIL_BODY_FIELD);
        return driver.findElement(MAIL_BODY_FIELD).getText();
    }
}
