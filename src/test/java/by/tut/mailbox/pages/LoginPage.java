package by.tut.mailbox.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPage extends BasePage {

    private final String LOGIN_PAGE_URL = "https://mail.tut.by/";
    private static final String USERNAME_SELECTOR = "Username";
    private static final String PASSWORD_SELECTOR = "Password";
    private static final String enterButtonSelector = "//input[@value=\"Войти\"]";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = USERNAME_SELECTOR)
    private WebElement userName;

    @FindBy(id = PASSWORD_SELECTOR)
    private WebElement password;

    @FindBy(xpath = enterButtonSelector)
    private WebElement enterButton;

    public LoginPage enterEmail(String userName) {
//        waitForVisible(driver, By.id(USERNAME_SELECTOR));
        logger.info("Enter userName: " + userName);
        this.userName.sendKeys(userName);
        return this;
    }

    public LoginPage enterPassword(String pass) {
//       waitForVisible(driver, By.name(PASSWORD_SELECTOR));
        logger.info("Enter password: " + pass);
        password.sendKeys(pass);
        return this;
    }

    public MailBoxPage submitLogin() {
        logger.info("Submit login form.");
        enterButton.click();
        waitForPageLoad();
        return new MailBoxPage(this.driver);
    }

    public LoginPage open() {
        this.driver.navigate().to(LOGIN_PAGE_URL);
        return this;
    }
}
