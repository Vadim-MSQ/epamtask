package by.tut.mailbox.pages;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class BasePage {

    protected WebDriver driver;
    protected Logger logger;

    public BasePage(WebDriver driver) {
        this.driver = driver;
        this.logger = LogManager.getLogger(this.getClass());
        waitForPageLoad();
        PageFactory.initElements(this.driver, this);
    }

    public WebDriver getWD() {
        return this.driver;
    }

    public boolean isElementPresent(By locator) {
        return driver.findElements(locator).size() > 0;
    }

    void waitForPageLoad(){
        WebDriverWait wait = new WebDriverWait(this.driver, 30);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
            }});
    }
    void waitForElementPresent(By locator){
        (new WebDriverWait(this.driver, 15))
                .until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    void waitForElementClickable(By locator){
        (new WebDriverWait(this.driver, 15))
                .until(ExpectedConditions.elementToBeClickable(locator));
    }

    void clickOnElementWithJS(WebElement element){
        JavascriptExecutor executor = (JavascriptExecutor)this.driver;
        executor.executeScript("arguments[0].click();", element);
    }



}

