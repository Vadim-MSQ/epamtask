package by.tut.mailbox.dataProviders;

import by.tut.mailbox.businessObjects.Account;

import java.sql.*;
import java.util.ArrayList;

public class SQLDBHelper {
    // JDBC driver name and database URL
    private static final String DB_URL = "jdbc:mysql://localhost:3306/epamtask";
    //  Database credentials
    private static final String USER = "admin";
    private static final String PASS = "axKGVdwji8YsWv1u";

    public ArrayList<Account> getDataFromDB(String sql) {
        ArrayList<Account> accounts = new ArrayList<>();
        try (
                Connection conn = DriverManager.getConnection(DB_URL, USER, PASS);
                Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt.executeQuery(sql);
        ) {
            accounts = getAccountsFromResult(rs);
        } catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return accounts;
    }

    private static ArrayList<Account> getAccountsFromResult(ResultSet result) throws SQLException {
        ArrayList<Account> accounts = new ArrayList<>();
        while (result.next()) {
            String login = result.getString("login");
            String password = result.getString("password");
            accounts.add(new Account(login, password));
        }
        return accounts;
    }
}
