package by.tut.mailbox.dataProviders;

import by.tut.mailbox.businessObjects.Account;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVHelper {

    private String pathToCSV = "src\\test\\resources\\accounts.csv";

    public List<Account> getUsers() {
        List<Account> accountList = new ArrayList<>();
        try {
            File file = new File(pathToCSV);
            Iterable<CSVRecord> records = CSVFormat.RFC4180.withHeader("login", "password").parse(new FileReader(file));
            for (CSVRecord record : records) {
                Account newAccount = new Account();
                newAccount.setLogin(record.get("login"));
                newAccount.setPassword(record.get("password"));
                accountList.add(newAccount);
            }
        } catch (IOException e) {
            e.getMessage();
        }
        return accountList;
    }
}
