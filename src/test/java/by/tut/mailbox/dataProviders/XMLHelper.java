package by.tut.mailbox.dataProviders;

import by.tut.mailbox.businessObjects.Account;
import by.tut.mailbox.businessObjects.Accounts;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.util.List;

public class XMLHelper {

    private String pathToXML = "src\\test\\resources\\accounts.xml";

    public List<Account> getDataFromXML() {

        Accounts accounts = new Accounts();
        try {
            JAXBContext context = JAXBContext.newInstance(Accounts.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            accounts = (Accounts) unmarshaller.unmarshal(new File(pathToXML));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
        return accounts.getAccounts();
    }
}


