package by.tut.mailbox.dataProviders;

import by.tut.mailbox.businessObjects.Account;
import org.apache.commons.collections4.ListUtils;
import org.testng.annotations.DataProvider;
import org.testng.collections.Lists;

import java.util.Arrays;
import java.util.List;

public class AccountsDataProvider {

    @DataProvider(name = "AccountsFromAllStorages")
    public Object[][] dp() {
        List<Object[]> result = Lists.newArrayList();
        result.addAll(Arrays.asList(getAccountsFromCSV()));
        result.addAll(Arrays.asList(getAccountsFromXML()));
        result.addAll(Arrays.asList(getAccountsFromDB()));
        return result.toArray(new Object[result.size()][]);
    }

    @DataProvider(name = "dataFromCSV")
    public Object[][] getAccountsFromCSV(){
        CSVHelper csvHelper = new CSVHelper();
        List<Account> accounts = csvHelper.getUsers();

        return formObjectsArrays(accounts);
    }

    @DataProvider(name = "dataFromXML")
    public Object[][] getAccountsFromXML(){
        XMLHelper xmlHelperelper = new XMLHelper();
        List<Account> accounts = xmlHelperelper.getDataFromXML();

        return formObjectsArrays(accounts);
    }

    @DataProvider(name = "dataFromDB")
    public Object[][] getAccountsFromDB(){
        SQLDBHelper sqldbHelper = new SQLDBHelper();
        List<Account> accounts = sqldbHelper.getDataFromDB("SELECT * FROM accounts");

        return formObjectsArrays(accounts);
    }

    private Object[][] formObjectsArrays(List<Account> accounts) {
        List<List<Account>> partition = ListUtils.partition(accounts, 2);
        Object[][] result = new Object[partition.size()][2];
        for(int i = 0; i < partition.size(); i++){
            result[i][0] = partition.get(i).get(0);
            result[i][1] = partition.get(i).get(1);
        }
        return result;
    }


}
