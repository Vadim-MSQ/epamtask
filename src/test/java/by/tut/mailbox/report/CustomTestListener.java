package by.tut.mailbox.report;

import by.tut.mailbox.tests.BaseTest;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class CustomTestListener implements ITestListener {

    private Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public void onTestStart(ITestResult iTestResult) {
        logger.info("Test '{}' started.", iTestResult.getName());
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        logger.info("Test '{}' PASSED.", iTestResult.getName());
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        logger.error("Test '{}' FAILED.", iTestResult.getName());
        String methodName = iTestResult.getName();
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("ddMMyyyy_hhmmss");
        Object currentClass = iTestResult.getInstance();
        WebDriver driver = ((BaseTest) currentClass).getWD();
        File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        try {
            String screenshotName = formater.format(calendar.getTime()) + "-FAILED-" + methodName + ".png";
            File destFile = new File("./logs/screenshots/" + screenshotName);
            FileUtils.copyFile(screenshot, destFile);
            logger.info("Saved screenshot: " + screenshotName);
        } catch (IOException e) {
            logger.error("Failed to make screenshot", e.getMessage());
        }
    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
    }

    @Override
    public void onStart(ITestContext iTestContext) {

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }

}
