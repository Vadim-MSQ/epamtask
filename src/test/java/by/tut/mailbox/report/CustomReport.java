package by.tut.mailbox.report;

import by.tut.mailbox.businessObjects.Account;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.testng.ITestResult;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class CustomReport {

    private static Logger logger = LogManager.getLogger(CustomReport.class);
    private String accounts;
    private String testResult;
    private String failureReason;

    public CustomReport(ITestResult testResult) {
        Object[] parameters = testResult.getParameters();
        Account[] accounts = Arrays.copyOf(parameters, parameters.length, Account[].class);

        this.accounts = Arrays.toString(accounts);
        this.testResult = testResult.isSuccess() ? "PASSED" : "FAILED";
        this.failureReason = testResult.getThrowable() != null ? testResult.getThrowable().toString() : "";
    }

    public static void saveReport(List<CustomReport> reportList) {
        HSSFWorkbook reportBook = new HSSFWorkbook();
        HSSFSheet sheet = reportBook.createSheet("Test Results");
        //create head row with column names
        HSSFRow rowHead = sheet.createRow(0);
        sheet.setColumnWidth(0, 90 * 256);
        sheet.setColumnWidth(1, 13 * 256);
        sheet.setColumnWidth(2, 200 * 256);
        rowHead.createCell(0).setCellValue("Accounts");
        rowHead.createCell(1).setCellValue("Result");
        rowHead.createCell(2).setCellValue("Failure reason");
        int rowCount = 1;
        for (CustomReport report : reportList) {
            HSSFRow newRow = sheet.createRow(rowCount);
            newRow.createCell(0).setCellValue(report.getAccounts());
            newRow.createCell(1).setCellValue(report.getTestResult());
            newRow.createCell(2).setCellValue(report.getFailureReason());
            rowCount++;
        }

        try {
            saveReportToDrive(reportBook);
            logger.info("Test Report was created.");
        } catch (IOException e) {
            logger.error("Failed to create test report. " + e.getMessage());
        }

    }

    private static void saveReportToDrive(HSSFWorkbook reportBook) throws IOException {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formater = new SimpleDateFormat("ddMMyyyy-hhmmss");
        String filename = "Test Report " + formater.format(calendar.getTime()) + ".xls";

            File reportFolder = new File("./logs/reports");
            if (!reportFolder.exists()) {
                reportFolder.mkdirs();
            }
            FileOutputStream fileOut = new FileOutputStream(reportFolder + File.separator + filename);
            reportBook.write(fileOut);
            fileOut.close();

    }

    public String getAccounts() {
        return accounts;
    }

    public void setAccounts(String accounts) {
        this.accounts = accounts;
    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }


}
