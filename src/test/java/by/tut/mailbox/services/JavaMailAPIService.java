package by.tut.mailbox.services;

import by.tut.mailbox.businessObjects.Account;
import by.tut.mailbox.businessObjects.Letter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class JavaMailAPIService {

    private Logger logger = LogManager.getLogger(this.getClass());
    private final String HOST = "smtp.yandex.ru";
    private Account sender;
    private Account recepient;

    public JavaMailAPIService(Account sender, Account recepient) {
        this.sender = sender;
        this.recepient = recepient;
    }

    public Letter createLetter() {
        logger.info("Creating letter.");
        Letter letter = new Letter();
        letter.setRecipient(recepient.getLogin());
        Properties letterProperties = getLetterProperties();
        letter.setSubject(letterProperties.getProperty("subject"));
        letter.setBody(letterProperties.getProperty("body"));
        return letter;
    }

    private Properties getLetterProperties() {
        Properties prop = new Properties();
        try (InputStream input = new FileInputStream("src\\test\\resources\\letter.properties");) {
            // load a properties file
            prop.load(input);
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
        return prop;
    }

    public void sendLetter(Letter letter) {
        final String login = sender.getLogin();
        final String password = sender.getPassword();

        logger.info("Sending letter from '{}' to '{}'.", login, letter.getRecipient());

        //Get the session object
        Properties props = new Properties();
        props.put("mail.smtp.host", HOST);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.quitwait", "false");
//        props.put("mail.debug", "true");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(login, password);
                    }
                });
//        session.setDebug(true);

        //Compose the message
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(sender.getLogin()));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(letter.getRecipient()));
            message.setSubject(letter.getSubject());
            message.setText(letter.getBody());

            Transport.send(message);
            logger.info("Letter sent successfully.");
        } catch (MessagingException e) {
            logger.error("Failed to sent letter. " + e.getMessage());
        }
        try {
            saveLetterIntoSentFolder(session, message);
            logger.info("Letter saved successfully.");
        } catch (MessagingException e) {
            logger.error("Failed to save letter. " + e.getMessage());
        }
    }

    private void saveLetterIntoSentFolder(Session session, Message message) throws MessagingException {
        // Copy letter to "Sent" folder
        Store store = session.getStore("imaps");
        store.connect("imap.yandex.ru", sender.getLogin(), sender.getPassword());
        Folder folder = store.getFolder("Отправленные");
        if (!folder.exists()) {
            folder.create(Folder.HOLDS_MESSAGES);
            folder.open(Folder.READ_WRITE);
        }
        try {
            folder.appendMessages(new Message[]{message});
        } finally {
            store.close();
        }
    }
}
