package by.tut.mailbox.services;

import by.tut.mailbox.businessObjects.Account;
import by.tut.mailbox.pages.LoginPage;
import by.tut.mailbox.pages.MailBoxPage;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;

public class MailboxService {
    private LoginPage loginPage;
    private MailBoxPage mailBoxPage;

    private WebDriver driver;

    public MailboxService(WebDriver driver) {
        this.driver = driver;
    }


    public void loginToMailbox(Account account) {
        loginPage = new LoginPage(driver);
        loginPage.open()
                .enterEmail(StringUtils.substringBefore(account.getLogin(), "@"))
                .enterPassword(account.getPassword());
        this.mailBoxPage = loginPage.submitLogin();
    }

    public String getPageTitle(){
        return this.driver.getTitle();
    }


    public String getUserName() {
        return mailBoxPage.getUserName();
    }

    public String getMailRecipient() {
        return mailBoxPage.getMailRecipient();
    }

    public String getMailSubject() {
        return mailBoxPage.getMailSubject();
    }

    public String getMailBody() {
        return mailBoxPage.getMailBody();
    }

    public void redirectToSentFolder() {
        mailBoxPage.switchToSent();
    }

    public void redirectToInboxFolder() {
        mailBoxPage.switchToInbox();
    }
}
