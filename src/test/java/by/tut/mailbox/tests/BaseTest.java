package by.tut.mailbox.tests;

import by.tut.mailbox.businessObjects.Account;
import by.tut.mailbox.businessObjects.Letter;
import by.tut.mailbox.report.CustomReport;
import by.tut.mailbox.services.JavaMailAPIService;
import by.tut.mailbox.services.WDManager;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;

public class BaseTest {
    WebDriver driver;
    private WDManager manager;
    private List<CustomReport> reportList;

    @Parameters({"user1", "pass1", "user2", "pass2"})
    @BeforeClass
    public void sendLetterViaJavaMailAPI(String user1, String pass1, String user2, String pass2) {
        reportList = new ArrayList<>();
        Account sender = new Account(user1, pass1);
        Account recipient = new Account(user2, pass2);
        JavaMailAPIService mailAPIService = new JavaMailAPIService(sender, recipient);
        Letter letter = mailAPIService.createLetter();
        mailAPIService.sendLetter(letter);
    }

    @Parameters({"browserName"})
    @BeforeMethod(alwaysRun = true)
    public void setUp(@Optional("chrome") String browserName) {
        manager = new WDManager();
        driver = manager.getWD(browserName);
    }

    @AfterMethod(alwaysRun = true)
    public void logTestResult(ITestResult testResult) {
        reportList.add(new CustomReport(testResult));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

    @AfterSuite
    public void saveReport() {
        CustomReport.saveReport(reportList);
    }

    public WebDriver getWD() {
        return driver;
    }
}
