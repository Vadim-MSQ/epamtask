package by.tut.mailbox.tests;

import by.tut.mailbox.businessObjects.Account;
import by.tut.mailbox.dataProviders.AccountsDataProvider;
import by.tut.mailbox.services.MailboxService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class MailBoxTest extends BaseTest {

    @Test(dataProvider = "AccountsFromAllStorages", dataProviderClass = AccountsDataProvider.class)
    public void checkLetterWasRecieved(Account sender, Account recipient) {
        MailboxService mailboxService = new MailboxService(driver);

        mailboxService.loginToMailbox(recipient);

        Assert.assertEquals(mailboxService.getMailRecipient(), sender.getLogin(), "INCORRECT SENDER!");
    }

    @Test(dataProvider = "AccountsFromAllStorages", dataProviderClass = AccountsDataProvider.class)
    public void checkLetterWasSent(Account sender, Account recipient) {
        MailboxService mailboxService = new MailboxService(driver);

        mailboxService.loginToMailbox(sender);
        mailboxService.redirectToSentFolder();

        Assert.assertEquals(mailboxService.getMailRecipient(), recipient.getLogin(), "INCORRECT RECIPIENT!");
    }



}
